import React, { useState } from "react"
import 'antd/dist/antd.css';
import { Form, Input, Button, Select, UploadProps } from 'antd';
import { Upload, message } from 'antd';
import { User } from '../../entities';
import { UploadOutlined } from '@ant-design/icons';
import { UploadFile } from 'antd/lib/upload/interface';
import axios from "axios";

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
/* eslint-disable no-template-curly-in-string */

const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not a valid email!',
        number: '${label} is not a valid number!',
        image: '${label} is not a png!',
        file: '${label} is not a pdf!'
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};


interface Props {
    values: User;
}

interface targetType {
    target: HTMLInputElement;
}

interface eventType {
    onChangeAssignmentStatus: (
        selectType: string,
        value: React.ChangeEvent<HTMLSelectElement>
    ) => void;
}
interface IProps {
    handleSearchTyping(event: React.FormEvent<HTMLInputElement>): void;
}








export function Register() {


    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const roles = ["Candidat", "Recruteur"];
    const [selectedId, setSelectedId] = useState(roles[0]);

    const [images, setImages] = useState<string[]>([]);

    const [pdf, setPDF] = useState('')

    const [state, setState] = useState<{ fileList: UploadFile[] }>({
        fileList: []
    });
    const [statePDF, setStatePDF] = useState<{ fileListPDF: UploadFile[] }>({
        fileListPDF: []
    });


    const [errorMessage, setErrorMessage] = useState(null);
    const [successfulRegister, setsuccessfulRegister] = useState('');



    const addUser = (values: any) => {

        setErrorMessage(null);
        console.log("PDF", pdf);
        console.log("IMAGE", images);
        
        
        try {

            const res = axios.post('http://localhost:8000/api/user/register', values)
                .then(res => {
                    setsuccessfulRegister(res.statusText)
                })
                .catch(res => {
                    console.log("res", res);
                    console.log("reserror", res.response.data.error);

                    setErrorMessage(res.response.data.error);
                })


        }
        catch (res: any) {
            console.log("res", res);

            setErrorMessage(res.error);
        }
    }


    const onFinish = async (values: any) => {

        const newUser = new FormData(); 


        newUser.append("name", values.name);
        newUser.append("role", values.role);
        newUser.append("email", values.email);
  
        newUser.append("password", values.password);
        await addUser({ ...values, file: images, pdf: pdf })

        console.log("values", { ...values, file: images, pdf: pdf });

    };

    function getBase64(file: any) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    const handleUpload = () => {
        const { fileList } = state;
        fileList.forEach(image => {
            getBase64(image)
                .then(result => {
                    setImages([...images, result as any])

                    return result
                })
                .catch(err => {
                    console.log(err);
                });


        })

        const { fileListPDF } = statePDF;
        fileListPDF.forEach(pdfBase => {
            getBase64(pdfBase)
                .then(result => {

                    setPDF(result as any)
                    return result
                })
                .catch(err => {
                    console.log(err);
                });
        });



    }




    const { fileList } = state;
    const props: UploadProps = {
        onRemove: imageFileName => {
            setState(state => {
                const index = state.fileList.indexOf(imageFileName as any);
                const newFileList = state.fileList.slice();
                newFileList.splice(index, 1);
                return {
                    fileList: newFileList,
                };
            });
        },
        beforeUpload: (file) => {
            const isPNG = file.type === 'image/png';
            if (!isPNG) {
                message.error(`${file.name} is not a png file`);
                return Upload.LIST_IGNORE;
            }
            setState(state => ({

                fileList: [...state.fileList, file],
            }));
            return false;
        },
        onChange: info => {
            console.log(info.fileList);
        },
        fileList,
    };

    const { fileListPDF } = statePDF;

    const propsPDF: UploadProps = {
        onRemove: pdfFileName => {
            setStatePDF(statePDF => {
                const indexPdf = statePDF.fileListPDF.indexOf(pdfFileName as any);
                const newFileListPDF = statePDF.fileListPDF.slice();
                newFileListPDF.splice(indexPdf, 1);
                return {
                    fileListPDF: newFileListPDF,
                };
            });
        },
        beforeUpload: (filePDF) => {
            setStatePDF(statePDF => ({
                fileListPDF: [...statePDF.fileListPDF, filePDF],
            }));
            return false;
        }

    };


    return (

        <div   >


            <Form action="/public" encType="multipart/form-data" method="post"
                labelCol={{
                    span: 4,
                }}
                wrapperCol={{
                    span: 14,
                }}
                layout="horizontal"
                onFinish={onFinish}
                validateMessages={validateMessages}


            >
                <Form.Item

                    name="name"
                    rules={[{ required: true, message: 'Please input your username!' }]}
                >
                    <Input onChange={event => setName(event.target.value)} placeholder="Name" />
                </Form.Item>
                <Form.Item

                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input onChange={event => setEmail(event.target.value)} placeholder="Email" />
                </Form.Item>

                <Form.Item

                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password placeholder="Password" onChange={event => setPassword(event.target.value)} />
                </Form.Item>


                <Form.Item name="role" rules={[{ required: true, message: 'Please select your role!' }]}>
                    <Select placeholder="Select an option" value={selectedId}
                        onChange={e => setSelectedId(e)} allowClear>
                        {roles.map((item, i) => (
                            <Select.Option value={item} key={i}>
                                {item}
                            </Select.Option>
                        ))}
                    </Select>
                </Form.Item>

                <Form.Item

                    name="image"
                    rules={[{ required: true, message: 'Please add two images!' }]}
                >
                    <Upload {...props}
                        maxCount={2}

                    >
                        <Button
                         disabled={

                            fileList.length > 1
                        } icon={<UploadOutlined />}>Add pictures (optional)</Button>
                    </Upload>

                </Form.Item>

                <Form.Item
                    name="pdf"
                    rules={[{ required: true, message: 'Please add a pdf!' }]}

                >
                    <Upload {...propsPDF}
                        maxCount={1}
                        onChange={handleUpload}

                    >
                        <Button 
                         disabled={

                            fileListPDF.length === 1
                        } icon={<UploadOutlined />}>Add a CV (optional)</Button>
                    </Upload>

                </Form.Item>




                <Form.Item>
                    <Button htmlType="submit"  >Send</Button>
                </Form.Item>
            </Form>
            {errorMessage && (
                <p className="error"> {errorMessage} </p>
            )}
            {successfulRegister && (
                <p className="error"> {successfulRegister} </p>
            )}
        </div>

    );
}


