import Link from "next/link";
import { useRouter } from "next/router";
import styles from './Navbar.module.css'


export const Navbar = () => {

    const router = useRouter();
    /*     console.log("router is1", router.asPath); */

    return (
        <ul>
            <li className={router.asPath === "/register" ? "activeNav" : ""} >
                <Link href="/register">Register</Link>
            </li>

            <li className={router.asPath === "/login" ? "activeNav" : ""}>
                <Link href="/login">Login</Link>
            </li>
        </ul>
    );
};
