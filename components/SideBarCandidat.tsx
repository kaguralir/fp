import Link from "next/link";
import { useRouter } from "next/router";
import styles from './Navbar.module.css'
import { Icon } from '@iconify/react';
import { CloseOutlined, MessageOutlined, UserOutlined, CalendarOutlined, HomeFilled, FolderOpenOutlined, LogoutOutlined } from '@ant-design/icons'
import { Spin, Space } from 'antd';
import { signOut } from "next-auth/react";
export const SideBarCandidat = () => {




    return (<>


        <div className="nav show-menu" id="navbar">
            <nav className="nav__container">
                <div>
                    <a href="#" className="nav__link nav__logo">
                        <img className="logo_sideNavbar" src="https://i.ibb.co/TbFtMg2/Group-72-1.png" />          </a>

                    <div className="nav__list">
                        <div className="nav__items">
                            <h3 className="nav__subtitle">MENU</h3>
                            <a href="#" className="nav__link active">

                                <span className="nav__name nav__subtitle"><Link href="/"><div className="div_item"> <img className="icons_sibeBarCandidat" src="https://i.ibb.co/CvVbG8f/home.png" /> <p>Home</p></div></Link></span>
                            </a>


                            <a href="#" className="nav__link">

                                <span className="nav__name nav__subtitle"><Link href="/interface/candidat/EditBox"><div className="div_item"> <img className="icons_sibeBarCandidat" src="https://i.ibb.co/MSbxnw7/new.png" /> <p>My search</p></div></Link></span>

                            </a>
                            <a href="#" className="nav__link">

                                <span className="nav__name nav__subtitle"><Link href="/interface/profile/profile"><div className="div_item"> <img className="icons_sibeBarCandidat" src="https://i.ibb.co/mqQB6fQ/user.png" /> <p>My profile</p></div></Link></span>

                            </a>
                            {/* 
                            <a href="#" className="nav__link">

                                <span className="nav__name nav__subtitle"><Link href="/interface/matches/matches"><div className="div_item"> <img className="icons_sibeBarCandidat" src="https://i.ibb.co/C7DdbcQ/calendar.png" /><p>Kanban</p></div></Link></span>
                            </a> */}
                            <a href="#" className="nav__link">

                                <span className="nav__name nav__subtitle"><Link href="/interface/matches/Messages"><div className="div_item"> <img className="icons_sibeBarCandidat" src="https://i.ibb.co/8K8KqY8/chat.png" /> <p>Messages</p></div></Link></span>
                            </a>

                            <a href="#" className="nav__link nav__logout">
                                <span className="nav__name nav__subtitle"><div className="div_item" onClick={() => signOut()}> <img className="icons_sibeBarCandidat" src="https://i.ibb.co/pL1zCqs/exit.pngg" /> <p>Sign out</p></div></span>
                            </a>
                        </div>

                    </div>
                </div>


            </nav>
        </div >

        <script src="assets/js/main.js"></script>
    </>

    );
};
