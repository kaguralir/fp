import { Button, Radio } from 'antd';
import { DownloadOutlined } from '@ant-design/icons';
import { PlusCircleTwoTone } from '@ant-design/icons';
import { Tooltip } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router';
import {
    EditOutlined,
    EllipsisOutlined,
    PlusSquareTwoTone
} from "@ant-design/icons";
export const AntTest = () => {
    const router = useRouter();
    /*     console.log("router is1", router.asPath); */

    return (
        <div>
            <div>
                <Tooltip title="search">
                    <Button type="primary" shape="circle" icon={<SearchOutlined />} />
                </Tooltip>
                <Button type="primary" shape="circle">
                    A
                </Button>
                <Button type="primary" icon={<SearchOutlined />}>
                    Search
                </Button>
                <Tooltip title="search">
                    <Button shape="circle" icon={<SearchOutlined />} />
                </Tooltip>
                <Button icon={<SearchOutlined />}>Search</Button>

                <Tooltip title="search">
                    <Button shape="circle" icon={<SearchOutlined />} />
                </Tooltip>
                <Button icon={<SearchOutlined />}>Search</Button>
                <Tooltip title="search">
                    <Button type="dashed" shape="circle" icon={<SearchOutlined />} />
                </Tooltip>

                <Button type="primary">Primary Button</Button>
                <Button>Default Button</Button>
                <Button type="dashed">Dashed Button</Button>

                <Button type="text">Text Button</Button>
                <Button type="link">Link Button</Button>
                <PlusSquareTwoTone />
            </div >
        </div >
    )
} 
