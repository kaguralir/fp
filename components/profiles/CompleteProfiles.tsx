import { Space, Spin, Cascader, DatePicker } from "antd";
import { useSession } from "next-auth/react";
import React, { useCallback, useEffect, useState } from "react";
import { Form, Input, Button, Select, UploadProps } from 'antd';

import { EditOutlined } from "@ant-design/icons";

import moment, { Moment } from "moment";
import api from "../../services/interceptor";
import { SideBarCandidat } from "../SideBarCandidat";
import { SearchedJob } from "../../entities";
import Tags from "../jobs/Tags";


import countriesList from '../../pages/interface/recruteur/jsonData/countriesList.json'

import { PlusOutlined } from "@ant-design/icons";
const countriesListData: any = countriesList;
const { Option } = Select;
interface Props {
    values: SearchedJob;
}

export default function CompleteProfiles() {
    const [entries, setEntries] = useState<SearchedJob[]>([])
    const [description, setDesc] = useState<string>('')
    const [job_title, setJob_title] = useState<string>('')

    const [skill1, setSkill1] = useState<string>('')
    const [skill2, setSkill2] = useState<string>('')
    const [skill3, setSkill3] = useState<string>('')

    const [hobby1, setHobby1] = useState<string>('')
    const [hobby2, setHobby2] = useState<string>('')
    const [hobby3, setHobby3] = useState<string>('')

    const [softSkill1, setsoftSkill1] = useState<string>('')
    const [softSkill2, setsoftSkill2] = useState<string>('')
    const [softSkill3, setsoftSkill3] = useState<string>('')


    const [beginDate, setDate] = useState<Moment>()
    const dateFormat = "YYYY-MM-DD";

    const [indexToEdit, setIndexToEdit] = useState(-1);
    const { data: session, status } = useSession()
    const loading = status === 'loading'
    const user = session?.user as any;
    const id = user?.user_id;



    //countries
    const countries: any = []

    for (let value in countriesListData) {
        countries.push(
            <Option key={value}>{value}</Option>
        )

    }

    let valueCities: any = []

    const [selectedCity, selectedCityId] = useState(valueCities[0]);
    const [selectedCountry, selectedCountryId] = useState(countries[0]);
    const [chooseCity, setCityList] = useState<string[]>([])

    if (!id || !user || !session) {
        return <div><Space>
            <Spin size="large" />
        </Space> </div>
    }
    if (!user || user.isLoggedIn === false) {
        return <div>Loading...</div>
    }


    const changeCountry = (e: any) => {
        selectedCountryId(
            [e][0]
        );
        setCityList(countriesListData[[e][0]])

    }




    const changeCity = (e: any) => {
        selectedCityId(e
        );
        console.log("changed city", e);

    }

    const addSearch = async (job_title: string, description: string, selectedCity: string, selectedCountry: string, skill1: string, skill2: string, skill3: string, softSkill1: string, softSkill2: string, softSkill3: string, hobby1: string, hobby2: string, hobby3: string, beginDate: any) => {
        try {

            await api.post('/searchedJob/addSearch', { job_title, description, selectedCity, selectedCountry, skill1, skill2, skill3, softSkill1, softSkill2, softSkill3, hobby1, hobby2, hobby3, beginDate });

        }
        catch (err) {
            console.log(
                "addSearch", err
            );

        }

    }

    const onFinish = async () => {
        console.log("entries send", entries);


        console.log("desc send", selectedCity);
        console.log("desc send", selectedCountry);




        await addSearch(job_title, description, selectedCity, selectedCountry, skill1, skill2, skill3, softSkill1, softSkill2, softSkill3, hobby1, hobby2, hobby3, beginDate)

    }

    console.log("entries", entries);
    console.log("entries lefnth", entries.length);






    return (
        <>

            <div className="completeProfile_container">
                <SideBarCandidat />
                <div className="completeProfile_form_container">

                    <h1>Please complete your profile</h1>
                    <Form>


                        <Form.Item name="job_title"
                        >
                            <Input

                                type="text"

                                onChange={(val: any) => {
                                    setJob_title(val.target.value);
                                }}

                                addonBefore="Job Title"
                                placeholder="Enter title job"
                            />

                        </Form.Item>

                        <Form.Item >
                            <Input
                                name="desc"
                                type="text"
                                placeholder="Enter a description"

                                addonBefore="Description"
                                onChange={(val: any) => {
                                    val = val.target.value;
                                    setDesc(val);
                                }}
                                onBlur={() => {
                                    setIndexToEdit(-1);
                                }}
                            />
                        </Form.Item>

                        <Form.Item label="Countries" name="Country" >

                            <Select style={{ width: 90 }}
                                value={selectedCountry}
                                onChange={e => changeCountry(e)} allowClear filterOption={true} >
                                {countries}

                            </Select>

                        </Form.Item>

                        <Form.Item label="Cities" name="City" >

                            <Select value={selectedCity} allowClear onChange={e => changeCity(e)} filterOption={true}>
                                {chooseCity.map((item, i) => (
                                    <Select.Option addonBefore="City" value={item} key={i}>
                                        {item}
                                    </Select.Option>
                                ))}

                            </Select>

                        </Form.Item>

                        <Form.Item >
                            <DatePicker name="beginDate" defaultValue={moment(beginDate)} format={dateFormat}
                                onChange={(date: any, dateString: any) => setDate(dateString)} />

                        </Form.Item>
                        <Form.Item >
                            <Input
                                addonBefore="Skill"
                                name="skill1"
                                type="text"
                                placeholder="Enter a skill"


                                onChange={(val: any) => {

                                    setSkill1(val.target.value);
                                }}

                            />
                        </Form.Item>
                        <Form.Item >
                            <Input
                                addonBefore="Skill"
                                name="skill2"
                                type="text"
                                placeholder="Enter a skill"


                                onChange={(val: any) => {

                                    setSkill2(val.target.value);
                                }}

                            />
                        </Form.Item>
                        <Form.Item >
                            <Input
                                addonBefore="Skill"
                                name="skill3"
                                type="text"
                                placeholder="Enter a skill"


                                onChange={(val: any) => {

                                    setSkill3(val.target.value);
                                }}

                            />
                        </Form.Item>

                        <Form.Item >
                            <Input
                                addonBefore="Soft Skill"
                                name="softSkill1"
                                type="text"
                                placeholder="Enter a softSkill"


                                onChange={(val: any) => {

                                    setsoftSkill1(val.target.value);
                                }}

                            />
                        </Form.Item>
                        <Form.Item >
                            <Input
                                addonBefore="Soft Skill"
                                name="softSkill2"
                                type="text"
                                placeholder="Enter a softSkill"


                                onChange={(val: any) => {

                                    setsoftSkill2(val.target.value);
                                }}

                            />
                        </Form.Item>
                        <Form.Item >
                            <Input
                                addonBefore="Soft Skill"
                                name="softSkill3"
                                type="text"
                                placeholder="Enter a softSkill"


                                onChange={(val: any) => {

                                    setsoftSkill3(val.target.value);
                                }}

                            />
                        </Form.Item>

                        <Form.Item >
                            <Input
                                addonBefore="Hobby"
                                name="hobby1"
                                type="text"
                                placeholder="Enter a hobby"


                                onChange={(val: any) => {

                                    setHobby1(val.target.value);
                                }}

                            />
                        </Form.Item>
                        <Form.Item >
                            <Input
                                addonBefore="Hobby"
                                name="hobby2"
                                type="text"
                                placeholder="Enter a hobby"


                                onChange={(val: any) => {

                                    setHobby2(val.target.value);
                                }}

                            />
                        </Form.Item>
                        <Form.Item >
                            <Input
                                addonBefore="Hobby"
                                name="hobby3"
                                type="text"
                                placeholder="Enter a hobby"


                                onChange={(val: any) => {

                                    setHobby3(val.target.value);
                                }}

                            />
                        </Form.Item>


                    </Form>
                    <Button type="primary" htmlType="submit" onClick={onFinish}>Complete profile</Button>
                </div>
            </div>

        </>
    );
}