import React, { useState } from 'react';
import { Select } from 'antd';
import jobList from '../../pages/interface/recruteur/jsonData/jobs.json'


const JobSearchAutoComplete = () => {

    const job = jobList.jobsList.map((item, i) => (
        item

    ));

    const jobsCapitalized = job.map((eachJob: string, i) => (
        eachJob.charAt(0).toUpperCase() + eachJob.slice(1)
    ));



    const [selectedJobId, setSelectedJobId] = useState(job[0]);


    return (


        <Select showSearch placeholder="Select a job" value={selectedJobId.charAt(0).toUpperCase() + selectedJobId.slice(1)}
            onChange={e => setSelectedJobId(e)} allowClear filterOption={true} >
            {jobsCapitalized.map((item, i) => (
                <Select.Option value={item} key={i}>
                    {item}
                </Select.Option>
            ))}
        </Select>

    );
};
export default JobSearchAutoComplete;