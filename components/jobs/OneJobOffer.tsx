import React, { useState, useEffect, useCallback } from 'react';
import axios from 'axios';
import { getSession, signIn, useSession } from 'next-auth/react';
import { GetStaticPaths, GetStaticProps, NextApiRequest, NextApiResponse, NextPage } from 'next';

import { Menu, Dropdown, Button, message, Space, Tooltip, Divider, Tag, Spin } from 'antd';
import { DownOutlined, UserOutlined } from '@ant-design/icons';

import { Carousel, Radio } from "antd";
import { Card } from "antd";
import { DownloadOutlined } from "@ant-design/icons";
import { CheckCircleFilled, CloseCircleFilled, CheckOutlined, CloseOutlined, LoadingOutlined } from "@ant-design/icons";

import { Select } from 'antd';
import { jobOffer, ProfileCandidate, User } from '../../entities';
import api from '../../services/interceptor';
import { SideBarCandidat } from '../SideBarCandidat';
import { SideNavbar } from '../SideNavbar';


const { Option } = Select;



export default function OneJobOffer() {

    const { data: session, status } = useSession()
    const loading = status === 'loading'
    const user = session?.user as any;

    const [jobs, getJobs] = useState<jobOffer[]>([]);
    const [job, setSelectedJob] = useState<jobOffer[]>([])

    const [candidatesForJob, setCandidatesForJob] = useState<ProfileCandidate[]>([])
    const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
    const loadDataOnlyOnce2 = useCallback(() => {
        console.log(`I need ${status}!!`);
        if (status === 'authenticated') {
            console.log("status is authenticated");
            console.log("user is", user);


            return user;
        }
    }, [status]);

    const getCandidates = () => {
        try {

            console.log("job", job);

            api.get(`${process.env.NEXT_PUBLIC_API_URL}/interest/jobCandidatesWithoutInterest/${job}`).then((response) => {
                if (response) {
                    console.log("candidates", response.data.data);

                    setCandidatesForJob(response.data.data)

                    return;
                };
                if (!response) {
                    console.log("no joboffers data");

                }
            });
        }
        catch (err) {
            console.log("err", err);

        }

    }

    useEffect(() => {
        loadDataOnlyOnce2();
        try {
            api.get(`${process.env.NEXT_PUBLIC_API_URL}/jobOffers/getJobByRecruiter/${user.user_id}`).then((response) => {
                if (response) {
                    console.log("session is", response.data.data);

                    getJobs(response.data.data)


                    return;
                };
                if (!response) {
                    console.log("no joboffers data");

                }
            });
        }
        catch (err) {
            console.log("err", err);

        }


    }, []);



    useEffect(() => {

        loadDataOnlyOnce2();
        getCandidates()


    }, [job]);

    console.log("jobs", jobs);


    function onSearch(val: string) {
        console.log('search:', val);
    }



    // useEffect(() => {
    //     loadDataOnlyOnce2();
    //     fetchjobsforCandidates();
    // }, []);



    const interestPositive = (candidate_id: any, job: any, interest: any) => {
        try {

            api.post(`${process.env.NEXT_PUBLIC_API_URL}/interest/interestActivity`, { job, candidate_id, interest });
            console.log("values", job, candidate_id, interest);



        }
        catch (err) {
            console.log(
                "interestValue", err
            );

        }

    }


    const onFinish = async (candidate_id: any, job_id: any) => {
        const newInterest = new FormData();
        const interest = 1;
        newInterest.append("jobApplied_id", job_id);
        newInterest.append("candidateWhoApplied_id", candidate_id);
        newInterest.append("interest", interest.toString());

        await interestPositive(candidate_id, job_id, interest)
        getCandidates()


    };
    const onReject = async (candidate_id: any, job_id: any) => {
        const newInterest = new FormData();
        const interest = 0;
        newInterest.append("jobApplied_id", job_id);
        newInterest.append("candidateWhoApplied_id", candidate_id);
        newInterest.append("interest", interest.toString());

        await interestPositive(candidate_id, job_id, interest)
        getCandidates()


    };

    console.log("fileName", candidatesForJob);

    /*     if (candidatesForJob.length < 1) {
            return <div>Please add a search or wait.</div>
        }
     */
    return (
        <div className="editBox_container">
            <SideNavbar />
            <div className="big_update_container">
                <Select
                    showSearch
                    placeholder="Select a job search"
                    optionFilterProp="children"
                    onSearch={onSearch}
                    onChange={e => setSelectedJob(e)}                >
                    {jobs?.map((OneJob, i) =>
                        <Option name="jobOffer_id" key={OneJob.jobOffer_id} value={OneJob.jobOffer_id} allowClear filterOption={true}>{OneJob.jobOffer_role}</Option>
                    )}
                </Select>
                {candidatesForJob.length < 1 ?

                    <div>Please add a search or wait.</div>
                    :
                    <div className="secondContainerRecruiterHome">


                        <div key={candidatesForJob[0].user_id} className="card-width">


                            <div className="card-container">


                                {candidatesForJob[0].images ? candidatesForJob[0].images?.map((oneImage, i) =>
                                    <img

                                        className="round"
                                        src={`${process.env.NEXT_PUBLIC_URL}/uploads/thumbnails/${oneImage}`} />
                                )

                                    :

                                    null

                                }






                                <h3>{candidatesForJob[0].name}</h3>

                                <h3>
                                    {candidatesForJob[0].job_title}<br /> {candidatesForJob[0].description}
                                </h3>

                                <div className="skills">
                                    <ul>
                                        <li>{candidatesForJob[0].skill1}</li>
                                        <li>{candidatesForJob[0].skill2}</li>
                                        <li>{candidatesForJob[0].skill3}</li>

                                    </ul>
                                    <div>
                                        {" "}
                                        <Tag color="lime">{candidatesForJob[0].softSkill1}</Tag>
                                        <Tag color="green">{candidatesForJob[0].softSkill2}</Tag>
                                        <Tag color="cyan">{candidatesForJob[0].softSkill3}</Tag>


                                        <Tag color="magenta">{candidatesForJob[0].hobby1}</Tag>
                                        <Tag color="red">{candidatesForJob[0].hobby2}</Tag>
                                        <Tag color="volcano">{candidatesForJob[0].hobby3}</Tag>
                                    </div>

                                </div>
                                <div className="buttons">
                                    <Button
                                        onClick={() => onFinish(candidatesForJob[0].user_id, job)}

                                        type="primary"
                                        icon={<CheckOutlined />}
                                        size={"large"}
                                    />
                                    <Button

                                        onClick={() => onReject(candidatesForJob[0].user_id, job)}
                                        value={candidatesForJob[0].user_id}
                                        type="primary"
                                        shape="circle"
                                        icon={<CloseOutlined />}
                                        size={"large"}
                                    />

                                </div>
                            </div>
                        </div>

                    </div>
                }
            </div>

        </div>
    )
}


