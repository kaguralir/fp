import React, { useEffect, useState } from 'react';
import { Input, Select, Tag, Tooltip } from 'antd';

import countriesList from '../../pages/interface/recruteur/jsonData/countriesList.json'
import Form from 'antd/lib/form/Form';
const countriesListData: any = countriesList;
const Option = Select.Option;
import { PlusOutlined } from "@ant-design/icons";

interface countriesList {
    [key: string]: number
}

const Tags = () => {



    const [inputVisible, setinputVisible] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState<string>("");
    let [tags, setTags] = useState<string[]>([]);

    const handleClose = (removedTag: any) => {
        const newTags = tags.filter((tag) => tag !== removedTag);
        console.log("tags are", newTags);
        setTags(newTags);
    };


    const showInput = () => {
        setinputVisible(true);
    };

    const handleInputChange = (e: any) => {
        setInputValue(e.target.value);
    };

    const handleInputConfirm = () => {

        if (inputValue && tags.indexOf(inputValue) === -1) {
            tags = [...tags, inputValue];
        }
        console.log(tags);

        setTags(tags);
        setinputVisible(false);
        setInputValue('');

    };
    const saveInputRef = (input: any) => {
        input = input;
    };





    return (
        <>

<div className='tags_list_container'>
                                {tags.map((item, i) =>

                                    <div style={{ marginBottom: 16 }}>


                                        <Tag
                                            color="success"
                                            className="edit-tag"
                                            key={i}
                                            closable={i !== -1}
                                            onClose={() => handleClose(item)}


                                        >


                                            <Tooltip title={item} key={i}>
                                                {item.slice(0, 20)}
                                            </Tooltip>
                                        </Tag>
                                    </div>)}
                            </div>

                           
                                {tags.length <= 5 ? (
                                    <div>
                                        Yes

                                        {inputVisible && (
                                            <Input
                                                showCount maxLength={30}
                                                ref={saveInputRef}
                                                type="text"
                                                size="small"
                                                style={{ width: 278 }}
                                                value={inputValue}
                                                onChange={handleInputChange}
                                                onBlur={handleInputConfirm}
                                                onPressEnter={handleInputConfirm}
                                            />
                                        )}
                                        {!inputVisible && (
                                            <Tag color="success" onClick={showInput} className="site-tag-plus">
                                                <PlusOutlined /> Insert skills
                                            </Tag>
                                        )}
                                    </div>
                                ) : (
                                    <div> NO</div>
                                )}      

        </>

    );
};
export default Tags;
