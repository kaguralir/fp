import React, { useEffect, useState } from 'react';
import { Select } from 'antd';

import countriesList from '../../pages/interface/recruteur/jsonData/countriesList.json'
const countriesListData: any = countriesList;
const Option = Select.Option;

interface countriesList {
    [key: string]: number
}

const GetCity = () => {



    const countries: any = []

    for (let value in countriesListData) {
        countries.push(
            <Option key={value}>{value}</Option>
        )

    }

    let valueCities: any = []

    const [selectedCity, selectedCityId] = useState(valueCities[0]);
    const [selectedCountry, selectedCountryId] = useState(countries[0]);
    const [chooseCity, setCityList] = useState<string[]>([])




    const changeCountry = (e: any) => {
        selectedCountryId({
            valueCities: [e],
            countries: [e][0]
        });
        setCityList(countriesListData[[e][0]])

    }




    const changeCity = (e: any) => {
        selectedCityId({
            valueCities: e,
        });
        console.log("changed city", e);

    }



    return (
        <>


            <Select style={{ width: 90 }}
                value={selectedCountry[0]}
                onChange={e => changeCountry(e)} allowClear filterOption={true} >
                {countries}

            </Select>
            <br />
            <br />
            <br />


            <Select value={selectedCountry[0]} allowClear onChange={e => changeCity(e)} filterOption={true}>
                {chooseCity.map((item, i) => (
                    <Select.Option value={item} key={i}>
                        {item}
                    </Select.Option>
                ))}

            </Select>


        </>

    );
};
export default GetCity;
