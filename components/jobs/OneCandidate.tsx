import React, { useState, useEffect, useCallback } from 'react';
import axios from 'axios';
import { getSession, signIn, useSession } from 'next-auth/react';
import { GetStaticPaths, GetStaticProps, NextApiRequest, NextApiResponse, NextPage } from 'next';

import { Menu, Dropdown, Button, message, Space, Tooltip, Divider, Tag, Spin } from 'antd';
import { DownOutlined, UserOutlined } from '@ant-design/icons';

import { Carousel, Radio } from "antd";
import { Card } from "antd";
import { DownloadOutlined } from "@ant-design/icons";
import { CheckCircleFilled, CloseCircleFilled, CheckOutlined, CloseOutlined } from "@ant-design/icons";

import { Select } from 'antd';
import { jobOffer, User } from '../../entities';
import api from '../../services/interceptor';
import { SideBarCandidat } from '../SideBarCandidat';
import moment from 'moment';


const { Option } = Select;



export default function OneCandidate() {

    const { data: session, status } = useSession()
    const loading = status === 'loading'
    const user = session?.user as any;

    const [jobs, getJobs] = useState<jobOffer[]>();
    const [job, setSelectedJob] = useState<jobOffer[]>([])
    const [pictures, setPictures] = useState<string>();



    const [candidatesForJob, setCandidatesForJob] = useState<User[]>([])

    /* 
        if (typeof window !== 'undefined' && loading) return null
    
     */
    const loadDataOnlyOnce2 = useCallback(() => {
        console.log(`I need ${status}!!`);
        if (status === 'authenticated') {
            console.log("status is authenticated");
            console.log("user is", user);


            return user;
        }
    }, [status]);
    async function fetchjobsforCandidates() {
        try {
            api.get(`${process.env.NEXT_PUBLIC_API_URL}/jobOffers/getJobTestPerJob`, user.user_id).then((response) => {
                if (response) {
                    console.log("get job is", response.data.data);

                    getJobs(response.data.data)


                    return;
                };
                if (!response) {
                    console.log("no joboffers data");

                }
            });
        }
        catch (err) {
            console.log("err", err);

        }
    }

    useEffect(() => {
        loadDataOnlyOnce2();
        fetchjobsforCandidates();
    }, []);



    const interestPositive = (candidate_id: any, job: any, interest: any) => {
        try {

            api.post(`${process.env.NEXT_PUBLIC_API_URL}/interest/interestActivity`, { job, candidate_id, interest });
            console.log("values", job, candidate_id, interest);



        }
        catch (err) {
            console.log(
                "interestValue", err
            );

        }

    }


    const onFinish = async (candidate_id: any, job_id: any) => {
        const newInterest = new FormData();
        const interest = 1;
        newInterest.append("jobApplied_id", job_id);
        newInterest.append("candidateWhoApplied_id", candidate_id);
        newInterest.append("interest", interest.toString());

        await interestPositive(candidate_id, job_id, interest)
        await fetchjobsforCandidates()

    };
    const onReject = async (candidate_id: any, job_id: any) => {
        const newInterest = new FormData();
        const interest = 0;
        newInterest.append("jobApplied_id", job_id);
        newInterest.append("candidateWhoApplied_id", candidate_id);
        newInterest.append("interest", interest.toString());

        await interestPositive(candidate_id, job_id, interest)
        await fetchjobsforCandidates()


    };
    /*     {
            jobs?.map((job, i) => {
                job.tagDescription?.map((oneTag, i) => {
    
                    oneTag.map((oneTag, i) => {
    
                        oneTag.map((oneTag, i) =>
    
                            console.log('onetag', oneTag)
    
    
                        )
                    }
                    )
                }
                )
            }
            )
        }
     */



    return (
        <div className="editBox_container">
            <SideBarCandidat />

            {job?.length > 0 ?

            <div className="big_update_container">
                <div className="secondContainerRecruiterHome">


                    <div key={jobs ? jobs[0].jobOffer_id : null} className="card-width">


                        <div className="card-container">

                            {jobs ? jobs[0].images?.map((oneImage, i) =>
                                <Carousel dotPosition={"top"}>

                                    <img className="round" src={oneImage ? `${process.env.NEXT_PUBLIC_URL}uploads/thumbnails/${oneImage}` : 'https://images.unsplash.com/photo-1599508704512-2f19efd1e35f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80'} />
                                </Carousel>
                            )
                                : null}
                            <h3> {jobs ? jobs[0].jobOffer_role : null}</h3>
                            <h6>{jobs ? jobs[0].organizationName : null}</h6>
                            <p>
                                {jobs ? jobs[0].city : null} <br />{jobs ? jobs[0].country : null}
                            </p>
                            
                            <div className="skills">
                                <ul>
                                    <li>{jobs ? jobs[0].jobOffer_description : null}</li>
                                </ul>
                                {jobs ?
                                    <div>
                                        {" "}

                                        {
                                            jobs ? jobs[0].tagDescription?.map((oneTag, i) =>
                                                <div>{
                                                    oneTag.map((oneTag, i) =>
                                                        <Tag color="magenta">{oneTag.description}</Tag>



                                                    )
                                                }
                                                </div>

                                            )

                                                : null
                                        }

                                    </div>

                                    :

                                    <p>no tag</p>
                                }
                            </div>
                            <div className="buttons">
                                <Button
                                    onClick={() => onFinish(user.user_id, jobs ? jobs[0].jobOffer_id : null)}

                                    type="primary"
                                    icon={<CheckOutlined />}
                                    size={"large"}
                                />
                                <Button


                                    onClick={() => onReject(user.user_id, jobs ? jobs[0].jobOffer_id : null)}
                                    type="primary"
                                    shape="circle"
                                    icon={<CloseOutlined />}
                                    size={"large"}
                                />

                            </div>
                        </div>
                    </div>

                </div>

            </div>
            :
            <div>There are no offer for you yet or complete your profile.</div>}

        </div>
    )
}


