import React, { useState } from 'react';
import { Select } from 'antd';
import jobList from '../../pages/interface/recruteur/jsonData/jobs.json'


const JobSearchAutoComplete = () => {

    const job = jobList.jobsList.map((item, i) => (
        { item }
    ));
    const [selectedId, setSelectedId] = useState(job[0]);


    return (


        <Select showSearch placeholder="Select an option" value={selectedId}
            onChange={e => setSelectedId(e)} allowClear filterOption={true} >
            {jobList.jobsList.map((item, i) => (
                <Select.Option value={item} key={i}>
                    {item}
                </Select.Option>
            ))}
        </Select>

    );
};
export default JobSearchAutoComplete;