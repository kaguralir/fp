export interface User {
    user_id?: number;
    demo?: number;
    role?: string;
    name?: string;
    email?: string;
    password?: string;
    updatedAt?: Date;
    fileName?: Uploads[];
    pdfFileName?: Uploads[];
}
export interface ProfileCandidate {
    user_id: number;
    updatedAt?: Date;
    fileName?: Uploads[];
    images?: [];
    available: boolean;
    job_title: string;
    city: string;
    country: string;
    description: string;
    beginDate: Date;
    hobby1: string;
    hobby2: string;
    hobby3: string;
    name: string;
    remote: string;
    skill1: string;
    skill2: string;
    skill3: string;
    skill4: string;
    skill5: string;
    softSkill1: string;
    softSkill2: string;
    softSkill3: string;


}


export interface Uploads {
    upload_id?: number;
    userUploader_id?: number;
    user?: User;
    fileName?: string;
    pdfFileName?: string;
    jobOffer_id?: jobOffer;
    url?: string
    thumbnail?: string

}



export interface jobOffer {
    jobOffer_id: number;
    recruiter_id: number;
    remote: string;
    organizationName: string;
    jobOffer_role: string;
    jobOffer_description: string;
    country: string;
    city: string;
    updatedAt: Date;
    images?: Uploads[][];

    tagDescription: jobTags[][];
}

export interface oneJobOffer {
    jobOffer_id: number;
    available: number;
    recruiter_id: number;
    remote: string;
    organizationName: string;
    jobOffer_role: string;
    jobOffer_description: string;
    country: string;
    city: string;
    updatedAt: Date;
    images?: Uploads[][];

    tagDescription: jobTags[];
}



export interface jobTags {
    jobTags_id: number;
    job_id: number;
    description: string;
    job: jobOffer;
}

export interface SearchedJob {

    searchedJob_id: number;
    candidat_id: number;
    available: number;
    remote: string;
    beginDate: Date;
    city: string;
    country: string;
    car_ownership: number;
    job_title: string;
    description: string;
    projects: string;
    updatedAt: Date;
    candidateSkills: skillsEntity;
    skill1: string;
}


export interface skillsEntity {

    skills_id: number;
    jobSkills_id: number;
    candidatSkills_id: number;
    skill1: string;
    skill2: string;
    skill3: string;
    skill4: string;
    skill5: string;
    softSkill1: string;
    softSkill2: string;
    softSkill3: string;
    hobby1: string;
    hobby2: string;
    hobby3: string;
    user: User;
}
export interface Conversations {

    conversation_id: number;
    mutualInterest_id: number;
    sender_id: number;
    messageSend: string;
    sendDate: Date;

}


export interface Interest {
    interest_id?: number;
    jobApplied_id?: number;
    candidateWhoApplied_id?: number;
    recruiterJobOffer_id?: number;
    interest?: number;
    user?: User;
    images: Uploads[][];
    pdfs: Uploads;
    job: jobOffer;
    name:string;
    organizationName:string;
    jobOffer_description:string;

}


export interface Interest2 {
    interest_id?: number;
    jobApplied_id?: number;
    candidateWhoApplied_id?: number;
    recruiterJobOffer_id?: number;
    interest?: number;
    user?: User;
    images: Uploads[][];
    pdfs: Uploads;
    job: jobOffer;
    jobOffer_role: string;
    jobOffer_description: string;
    name: string;
    pdf: string;

}




/* export interface Transport {
    id?: number;
    name?: string;
    pricePerWeight?: number;
    orders?: Order[];
}

export interface Comment {
    id?: number;
    ratingNumber?: number;
    description?: string;
    date?: Date;
    user?: User;
    product?: Product;
}
 */