Ceci est une application [Next.js], les outils utilisés sont SQL (MariaDB), Nextjs, React (Hooks), Typescript, express, axios, bcrypt, jest, principalement.

## Présentation

[Le backend-end de cette application est disponible sur github (cliquez sur ce texte)](https://github.com/kaguralir/back) 



## Concept

![alt text](https://i.ibb.co/C8g0tsM/Projet-chef-d-oeuvre-Neksoo-7.png)

## Charte graphique

![alt text](https://i.ibb.co/Jmn2gBJ/Projet-chef-d-oeuvre-Neksoo-4.png)


## Maquette

![alt text](https://i.ibb.co/zmr41gK/Projet-chef-d-oeuvre-Neksoo-1.png)

## Diagramme UML

![alt text](https://i.ibb.co/0BMLkhK/Projet-chef-d-oeuvre-Neksoo-2.png)

## User case

![alt text](https://i.ibb.co/DVdp5Vj/Projet-chef-d-oeuvre-Neksoo-6.png)

## User stories

![alt text](https://i.ibb.co/8N4rWnm/Projet-chef-d-oeuvre-Neksoo.png)



