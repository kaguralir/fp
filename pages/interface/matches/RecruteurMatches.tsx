
import React, { useState, useEffect, useCallback } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';

import { useSession } from 'next-auth/react';

import {
    List,
    message,
    Avatar,
    Input,
    Button,
    Form,
    Skeleton,
    Divider
} from "antd";
import { Comment, Tooltip } from "antd";
import VirtualList from "rc-virtual-list";
import moment from "moment";
import InfiniteScroll from "react-infinite-scroll-component";
import { FilePdfOutlined } from "@ant-design/icons";
import api from '../../../services/interceptor';

import "antd/dist/antd.css";
import { SideBarCandidat } from '../../../components/SideBarCandidat';

import { Conversations, Interest, Interest2, jobOffer, Uploads } from '../../../entities';
/* import "./index.css"; */
import { Spin } from 'antd';
import { LoadingOutlined, CommentOutlined } from '@ant-design/icons';
import { SideNavbar } from '../../../components/SideNavbar';


const ContainerHeight = 400;

const { TextArea } = Input;


export default function RecruteurMatches() {

    const { data: session, status } = useSession()
    const loadings = status === 'loading'
    const user = session?.user as any;

    const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

    const [data, setData] = useState<Interest2[]>([]);

    const [loading, setLoading] = useState(false);

    const [conversation, setConversation] = useState<number>();
    const [conversationData, setConversationData] = useState<Conversations[]>([])


    const [interestId, setInterestId] = useState<number>();
    const [message, setMessage] = useState<string>('');
    const [senderId, setSenderId] = useState<number>();

    const [isActive, setActive] = useState(false);
    const [getId, setId] = useState<number>();


    const loadDataOnlyOnce2 = useCallback(() => {
        if (status === 'authenticated') {
            return user;
        }
    }, [status]);

    useEffect(() => {
        loadDataOnlyOnce2();
        const loadMoreData = async () => {

            let res = await api.get(process.env.NEXT_PUBLIC_API_URL + '/conversation/mutualInterest/' + user.user_id);
            const json = await res.data.data;

            setData(json);

        }


        loadMoreData()
            .catch(console.error);

    }, [loadDataOnlyOnce2]);





    const appendData = async () => {

        try {
            let res = await api.get(`${process.env.NEXT_PUBLIC_API_URL}/conversation/convoPerInterest/${conversation}`);
            const json = await res.data.data;

            setConversationData(json)


        }
        catch (err) {
            console.log("err", err);

        }


    }


    useEffect(() => {

        appendData();
    }, [conversation]);


    const onScroll = (e: any) => {
        if (e.target.scrollHeight - e.target.scrollTop === ContainerHeight) {
            appendData();
        }
    };
    if (!session) {
        return <div><h1>Access Denied</h1>
            <p>
                You must be signed in to view this page
            </p>
            <div><div><Spin indicator={antIcon} /></div></div></div>

    }
    const handleClose = (id: number) => {

        setConversation(id);
        setInterestId(id);

        setActive(!isActive);
        setId(id);



    };








    const addMessage = () => {


        const userId = user.user_id;
        try {
            let res = api.post(`${process.env.NEXT_PUBLIC_API_URL}/conversation/addMessage/`, { interestId, userId, message })

            appendData();


        }
        catch (err) {
            console.log("err front add message", err);
        }


    }


    const onChange = (e: any) => {

        setMessage(e.target.value)

    };


    const download = (pdf: any) => {
        console.log("clicked");

        const element = document.createElement("a");
        const dataBlob = new Blob([pdf], { type: "application/pdf" });
        const objectUrl = URL.createObjectURL(dataBlob);
        element.href = objectUrl;
        element.download = pdf;
        element.click();


    }


    return (


        <div className='big_container_messages'>
            {user.role === "Candidat" ?
                <SideBarCandidat />
                :
                <SideNavbar />
            }
            {data.length === 0 ?
                <div>You have no mutual interest yet.</div>
                :
                <div className="big_container_send_messages" >
                    <div className="container_messages">
                        <div className="list_container_interest">
                            <List >
                                <VirtualList
                                    data={data}
                                    height={ContainerHeight}
                                    itemHeight={47}
                                    itemKey="email"
                                    onScroll={onScroll}
                                >
                                    {(item,) => (
                                        <List.Item key={item.interest_id} onClick={() => handleClose(item?.interest_id as number)} className={`${item?.interest_id == getId ? "IsActive" : "inactive"}`}

                                        >


                                            <List.Item.Meta
                                                avatar={<Avatar src={item.images[0] ? `${process.env.NEXT_PUBLIC_URL}${item.images[0]}` : 'https://www.pikpng.com/pngl/m/29-298027_png-file-svg-anonymous-icon-clipart.png'} />}
                                                title={<a >{item.name}</a>}
                                                description={item.jobOffer_role}

                                            />



                                            {/*                                             <a href={`${process.env.NEXT_PUBLIC_URL}pdfs/${item.pdf}`} download={item.pdf}>Download</a>
 */}                                            <FilePdfOutlined onClick={() => window.open(`${process.env.NEXT_PUBLIC_URL}pdfs/${item.pdf}`)} />
                                            <CommentOutlined
                                            />


                                        </List.Item>
                                    )}
                                </VirtualList>
                            </List>
                        </div>
                        <div className="container_send_messages">
                            <div
                                id="scrollableDiv"
                                style={{
                                    height: 400,
                                    overflow: "auto",
                                    padding: "0 16px",
                                    border: "1px solid rgba(140, 140, 140, 0.35)"
                                }}
                            >
                                {conversationData.length === 0 ?

                                    <div> You didn't start a conversation with this user yet.</div>
                                    :
                                    <InfiniteScroll
                                        dataLength={conversationData.length}
                                        next={appendData}
                                        hasMore={data.length < 150}
                                        loader={<Skeleton avatar paragraph={{ rows: 1 }} active />}
                                        endMessage={<Divider plain>It is all, nothing more 🤐</Divider>}
                                        scrollableTarget="scrollableDiv"
                                    >
                                        <List
                                            className=' commentList_style'
                                            dataSource={conversationData}
                                            /*                                             onChange={e => setSenderId(item.sender_id)}
                                             */
                                            renderItem={(item, i) => (
                                                <List.Item key={i} className={item.sender_id === user.user_id ? 'sender_messages' : 'receiver_messages'}
                                                >
                                                    <Comment
                                                        className='comment_style'

                                                        author={<a  >{item.sender_id}</a>}
                                                        content={
                                                            <p>
                                                                {item.messageSend}

                                                            </p>

                                                        }
                                                        datetime={

                                                            <Tooltip title={moment(item.sendDate).format("YYYY-MM-DD HH:mm:ss")}>
                                                                <span >{moment(item.sendDate).fromNow()}</span>
                                                            </Tooltip>
                                                        }
                                                    />
                                                </List.Item>
                                            )}
                                        />
                                    </InfiniteScroll>
                                }
                            </div>

                            <div className="addcomment_container">
                                <Form.Item name="pdf">
                                    <TextArea showCount maxLength={100} rows={4} onChange={onChange} />
                                </Form.Item>
                                <Form.Item>
                                    <Button htmlType="submit" type="primary" onClick={() => addMessage()} >
                                        Add Comment
                                    </Button>
                                </Form.Item>
                            </div>
                        </div>

                    </div>
                </div>}

        </div>
    )
}
