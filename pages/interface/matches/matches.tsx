import React, { useState } from "react";


import { NextPage } from "next";
import { SideBarCandidat } from "../../../components/SideBarCandidat";


const Matches: NextPage<any> = () => {

    let dragged: any;
    const drag = (e: any) => {
        dragged = e.target;
    }
    const allowDrop = (e: any) => {
        e.preventDefault();
    }
    const drop = (e: any) => {
        e.preventDefault();

        let parent;
        if (e.currentTarget) {
            parent = e.currentTarget
        } else {

            parent = e.path.filter((i: any) => {
                if (i.classList) {
                    return i.classList.contains('kanban-list');
                }
            })[0];
        }

        parent.appendChild(dragged);
    }



    return (
        <div>
            <SideBarCandidat/>
            <div className="kanban">
                <div className="kanban-list" onDrop={drop} onDragOver={allowDrop}>
                    <div className="kanban-list-header">
                        <h4>List 1 header</h4>
                    </div>
                    <div className="kanban-item-container" draggable={true} onDragStart={drag}>
                        <div className="kanban-item">
                            <h5>Item 1</h5>
                        </div>
                    </div>
                    <div className="kanban-item-container" draggable={true} onDragStart={drag}>
                        <div className="kanban-item">
                            <h5>Item 2</h5>
                        </div>
                    </div>
                    <div className="kanban-item-container" draggable={true} onDragStart={drag}>
                        <div className="kanban-item">
                            <h5>Item 3</h5>
                        </div>
                    </div>
                </div>
                <div className="kanban-list" onDrop={drop} onDragOver={allowDrop}>
                    <div className="kanban-list-header">
                        <h4>List 2 header</h4>
                    </div>
                </div>
                <div className="kanban-list" onDrop={drop} onDragOver={allowDrop}>
                    <div className="kanban-list-header">
                        <h4>List 3 header</h4>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Matches;


