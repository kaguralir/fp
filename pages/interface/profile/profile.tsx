
import React, { useState, useEffect, useCallback } from 'react';
import { Image } from 'antd';
import { useSession } from 'next-auth/react';
import { Button, Tooltip } from "antd";
import { Spin, Space } from 'antd';
import { Tag, Form } from "antd";
import { DatePicker } from 'antd';
import { SideBarCandidat } from '../../../components/SideBarCandidat';
import { Input } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { Typography } from "antd";
import { withSuccess } from 'antd/lib/modal/confirm';
import api from '../../../services/interceptor';
import { SearchedJob, Uploads, User } from '../../../entities';
import moment from 'moment';
import axios from 'axios';
import { Upload } from 'antd';
import ImgCrop from 'antd-img-crop';
import { UploadFile } from 'antd/lib/upload/interface';
import { SideNav } from 'react-materialize';
import { SideNavbar } from '../../../components/SideNavbar';
const { Paragraph } = Typography;
declare let target: any


export default function Profile() {

    const { data: session, status } = useSession()
    const loading = status === 'loading'
    const user = session?.user as any;
    console.log("session is", session);
    console.log("status is", status);


    const [inputVisible, setinputVisible] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState<string>("");
    let [tags, setTags] = useState<string[]>([]);

    const remote = ["yes", "no"];
    const [selectedId, setSelectedId] = useState(remote[0]);
    const [beginDate, setBeginDate] = useState<string>("");
    const [car_ownership, setCar_ownership] = useState<number>(0);
    const [job_title, setJob_title] = useState<string>("");
    const [description, setDescription] = useState<string>("");
    const [projects, setProjects] = useState<string>("");
    const [searchedJob, setSearchedJob] = useState<SearchedJob[]>([])
    const [form] = Form.useForm();

    const [data, setData] = useState<SearchedJob[]>([])
    console.log("user.user_id", user?.user_id);
    const [profile, setProfile] = useState<User[]>([])

    const [pdf, setPDF] = useState<Uploads[]>()
    const [pictures, setPictures] = useState<{ fileList: UploadFile[] }>({
        fileList: []
    });

    const loadDataOnlyOnce2 = useCallback(() => {
        console.log(`I need ${status}!!`);
        if (status === 'authenticated') {
            console.log("status is authenticated");
            console.log("user is", user);


            return user;
        }
    }, [status]);


    async function fetchjobsforCandidates() {
        try {
            api.get(`${process.env.NEXT_PUBLIC_API_URL}/user/getProfile/` + user.user_id).then((response) => {
                if (response) {
                    console.log("get profile is", response.data.data);

                    setProfile(response.data.data)
                    {
                        profile.map((item, key) =>



                            setPDF(item.pdfFileName)

                        )
                    }
                    console.log("ITEM", pdf)

                    return;
                };
                if (!response) {
                    console.log("no profile data");

                }
            });
        }
        catch (err) {
            console.log("err", err);

        }
    }

    useEffect(() => {
        loadDataOnlyOnce2();
        fetchjobsforCandidates();
    }, [loadDataOnlyOnce2]);

    if (typeof window !== 'undefined' && loading) return null


    if (!session) {
      return <div><h1>Access Denied</h1>
        <p>
          You must be signed in to view this page
        </p></div>
    }


console.log("PROFILE IS", profile);



    return (

        <div className="edit_profile_container">

{user.role==="Recruteur" &&
      < SideNavbar />
      }
      {user.role==="Candidat" &&
      < SideBarCandidat/>
      }               
            
    
            <div className="profile_form_container">
                <h1>MY PROFILE</h1>
                {profile.map((item, key) =>
                    <div className="profile_content_container">

                        {item.fileName != null ? <Image.PreviewGroup>

                            <Image
                                width={200}
                                src={`${process.env.NEXT_PUBLIC_URL}uploads/thumbnails/${item.fileName}`}
                            />

                        </Image.PreviewGroup>
                            : null}



                    </div>



                )}

                {pdf ?
                    <div className="pdf">

                        <iframe src={`${process.env.NEXT_PUBLIC_URL}pdfs/${pdf}`} />
                        <a href={`${process.env.NEXT_PUBLIC_URL}pdfs/${pdf}`} target="_blank">Your CV</a>

                    </div>
                    :
                    null}
            </div>


        </div >
    )
}
