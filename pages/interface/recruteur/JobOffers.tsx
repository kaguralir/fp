import React, { useState } from "react"
import 'antd/dist/antd.css';
import { Form, Input, InputNumber, Button, Select, UploadProps, Tag, Tooltip } from 'antd';
import { User } from '../../../entities';
import api from "../../../services/interceptor";
import jobList from './jsonData/jobs.json'
import { useSession } from "next-auth/react";
import countriesList from './jsonData/countriesList.json'
import { SideNavbar } from "../../../components/SideNavbar";
import Link from "next/link";
import { PlusOutlined } from "@ant-design/icons";
const countriesListData: any = countriesList;
const Option = Select.Option;

interface countriesList {
    [key: string]: number
}
const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
/* eslint-disable no-template-curly-in-string */

const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not a valid email!',
        number: '${label} is not a valid number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};


interface Props {
    values: User;
}

interface targetType {
    target: HTMLInputElement;
}

interface eventType {
    onChangeAssignmentStatus: (
        selectType: string,
        value: React.ChangeEvent<HTMLSelectElement>
    ) => void;
}
interface IProps {
    handleSearchTyping(event: React.FormEvent<HTMLInputElement>): void;
}








export default function JobOffers() {


    const { data: session } = useSession()
    const user = session?.user as any;

    const [inputVisible, setinputVisible] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState<string>("");
    let [tags, setTags] = useState<string[]>([]);

    const [organizationName, setOrganizationName] = useState('');
    const [jobOffer_role, setJobOffer_role] = useState('');
    const [jobOffer_description, setJobOffer_description] = useState('');
    const [country, setCountry] = useState('');
    const [city, setcity] = useState('');

    const remoteOption = ["No", "Yes"];
    const [selectedId, setSelectedId] = useState(remoteOption[0]);





    const addJob = (values: any) => {
        try {

            api.post(process.env.NEXT_PUBLIC_API_URL + '/jobOffers/addJob', { values, tags });

        }
        catch (err) {
            console.log(
                "addJob", err
            );

        }

    }


    const onFinish = async (values: any) => {

        const newJob = new FormData();
        newJob.append("recruiter_id", user.user_id)
        newJob.append("remote", values.remote)
        newJob.append('organizationName', values.orgName)
        newJob.append('jobOffer_role', values.jobRole)
        newJob.append('jobOffer_description', values.jobDescription)
        newJob.append('country', values.jobCountry)
        newJob.append('city', values.jobCity)
        console.log("values", values);

        await addJob(values)


    };



    const countries: any = []

    for (let value in countriesListData) {
        countries.push(
            <Option key={value}>{value}</Option>
        )

    }

    let valueCities: any = []

    const [selectedCity, selectedCityId] = useState(valueCities[0]);
    const [selectedCountry, selectedCountryId] = useState(countries[0]);
    const [chooseCity, setCityList] = useState<string[]>([])




    const changeCountry = (e: any) => {
        selectedCountryId({
            valueCities: [e],
            countries: [e][0]
        });
        setCityList(countriesListData[[e][0]])

    }




    const changeCity = (e: any) => {
        selectedCityId({
            valueCities: e,
        });
        console.log("changed city", e);

    }

    const job = jobList.jobsList.map((item, i) => (
        item

    ));

    const jobsCapitalized = job.map((eachJob: string, i) => (
        eachJob.charAt(0).toUpperCase() + eachJob.slice(1)
    ));



    const [selectedJobId, setSelectedJobId] = useState(job[0]);

    /* TAG*/
    const handleClose = (removedTag: any) => {
        const newTags = tags.filter((tag) => tag !== removedTag);
        console.log("tags are", newTags);
        setTags(newTags);
    };


    const showInput = () => {
        setinputVisible(true);
    };

    const handleInputChange = (e: any) => {
        setInputValue(e.target.value);
    };

    const handleInputConfirm = () => {

        if (inputValue && tags.indexOf(inputValue) === -1) {
            tags = [...tags, inputValue];
        }
        console.log(tags);

        setTags(tags);
        setinputVisible(false);
        setInputValue('');

    };
    const saveInputRef = (input: any) => {
        input = input;
    };





    return (

        <div className="  editBox_container">
            <SideNavbar />

            <div className="big_update_container">
                <h3>Please complete form to create a new job offer : </h3>
                <div className="form_offer_container">
                <Form action="/public" encType="multipart/form-data" method="post"
                    labelCol={{
                        span: 4,
                    }}
                    wrapperCol={{
                        span: 14,
                    }}
                    layout="horizontal"
                    onFinish={onFinish}


                >
                    <Form.Item name="Remote">
                        <Select placeholder="Remote" value={selectedId}
                            onChange={e => setSelectedId(e)} allowClear>
                            {remoteOption.map((item, i) => (
                                <Select.Option value={item} key={i}>
                                    {item}
                                </Select.Option>
                            ))}
                        </Select>
                    </Form.Item>

                    <Form.Item
                        
                        name="orgName"

                    >
                        <Input placeholder="Organization name" onChange={event => setOrganizationName(event.target.value)} />
                    </Form.Item>

                    <Form.Item  name="jobRole"  >
                        <Select showSearch placeholder="Select a job" value={selectedJobId.charAt(0).toUpperCase() + selectedJobId.slice(1)}
                            onChange={e => setSelectedJobId(e)} allowClear filterOption={true} >
                            {jobsCapitalized.map((item, i) => (
                                <Select.Option value={item} key={i}>
                                    {item}
                                </Select.Option>
                            ))}
                        </Select>

                    </Form.Item>


                    <Form.Item
                        name="jobDescription"

                    >
                        <Input placeholder="Description"  onChange={event => setJobOffer_description(event.target.value)} />
                    </Form.Item>






                    <Form.Item  name="Country" >

                        <Select style={{ width: 90 }}
                            value={selectedCountry}
                            placeholder="Select a country" 
                            onChange={e => changeCountry(e)} allowClear filterOption={true} >
                            {countries}

                        </Select>

                    </Form.Item>

                    <Form.Item  name="City" >

                        <Select placeholder="Select a city"  value={selectedCity} allowClear onChange={e => changeCity(e)} filterOption={true}>
                            {chooseCity.map((item, i) => (
                                <Select.Option value={item} key={i}>
                                    {item}
                                </Select.Option>
                            ))}

                        </Select>

                    </Form.Item>

                    <div className='tags_list_container'>
                        {tags.map((item, i) =>

                            <div style={{ marginBottom: 16 }}>


                                <Tag
                                    color="success"
                                    className="edit-tag"
                                    key={i}
                                    closable={i !== -1}
                                    onClose={() => handleClose(item)}


                                >


                                    <Tooltip title={item} key={i}>
                                        {item.slice(0, 20)}
                                    </Tooltip>
                                </Tag>
                            </div>)}
                    </div>

                    <Form.Item>
                        {tags.length <= 5 ? (
                            <div>

                                {inputVisible && (
                                    <Input
                                        showCount maxLength={30}
                                        ref={saveInputRef}
                                        type="text"
                                        size="small"
                                        style={{ width: 278 }}
                                        value={inputValue}
                                        onChange={handleInputChange}
                                        onBlur={handleInputConfirm}
                                        onPressEnter={handleInputConfirm}
                                    />
                                )}
                                {!inputVisible && (
                                    <Tag color="success" onClick={showInput} className="site-tag-plus">
                                        <PlusOutlined /> Insert tags
                                    </Tag>
                                )}
                            </div>
                        ) : (
                            <div> No more tags can be added</div>
                        )} </Form.Item>


                    <Form.Item >

                        <Button htmlType="submit" >Create new offer</Button>

                    </Form.Item>
                </Form>
                </div>
            </div>
        </div>

    );
}
