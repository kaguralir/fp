import React, { useCallback, useEffect, useState } from "react"
import 'antd/dist/antd.css';
import { Form, Input, InputNumber, Button, Select, UploadProps, Tag, Tooltip, Space, Spin } from 'antd';
import { jobOffer, oneJobOffer, User } from '../../../../entities';
import api from "../../../../services/interceptor";
import jobList from '../jsonData/jobs.json'
import { useSession } from "next-auth/react";
import countriesList from '../jsonData/countriesList.json'
import { SideNavbar } from "../../../../components/SideNavbar";
import Link from "next/link";
import { PlusOutlined, EditOutlined } from "@ant-design/icons";
import { useRouter } from "next/router";
import { Entity } from "typeorm";
const countriesListData: any = countriesList;
const Option = Select.Option;
import { LoadingOutlined } from '@ant-design/icons';
import { Switch } from 'antd';
import { compareAsc, } from 'date-fns'
import { zonedTimeToUtc, utcToZonedTime, format } from 'date-fns-tz'



interface countriesList {
    [key: string]: number
}
const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
/* eslint-disable no-template-curly-in-string */

const validateMessages = {
    required: '${label} is required!',
    types: {
        email: '${label} is not a valid email!',
        number: '${label} is not a valid number!',
    },
    number: {
        range: '${label} must be between ${min} and ${max}',
    },
};


interface Props {
    values: User;
}

interface targetType {
    target: HTMLInputElement;
}

interface eventType {
    onChangeAssignmentStatus: (
        selectType: string,
        value: React.ChangeEvent<HTMLSelectElement>
    ) => void;
}
interface IProps {
    handleSearchTyping(event: React.FormEvent<HTMLInputElement>): void;
}








export default function JobDetails() {
    const router = useRouter()

    function useQuery() {
        const router = useRouter();
        const ready = router.asPath !== router.route;
        if (!ready) return null;
        return router.query;
    }
    const query = useQuery();
    const pid = router.query.id;
    const { data: session, status } = useSession()
    const loading = status === 'loading'
    const user = session?.user as any;
    const id = user?.user_id;
    const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;



    const [entries, setEntries] = useState<oneJobOffer[]>([]);
    const [indexToEdit, setIndexToEdit] = useState(-1);
    const [inputVisible, setinputVisible] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState<string>("");
    let [tags, setTags] = useState<string[]>([]);

    const [checked, setChecked] = useState<boolean>();

    const [organizationName, setOrganizationName] = useState('');
    const [jobOffer_role, setJobOffer_role] = useState('');
    const [jobOffer_description, setJobOffer_description] = useState('');
    const remoteOption = ["No", "Yes"];
    const [selectedId, setSelectedId] = useState(remoteOption[0]);

    const [date, setDate] = useState<String>();


    const loadDataOnlyOnce2 = useCallback(() => {
        console.log(`I need ${status}!!`);
        if (status === 'authenticated') {
            console.log("status is authenticated");
            console.log("user is", user);


            return user;
        }


    }, [status]);
    const noUser = () => {
        console.log("noUser");
        if (!id || !user || !session) {
            return <div><Space>
                <Spin size="large" />
            </Space> </div>
        }
        if (!user || user.isLoggedIn === false) {
            return <div>Loading...</div>
        }



    };
    useEffect(() => {
        loadDataOnlyOnce2();
        noUser();

        if (!query) {
            return;
        }
        console.log('my query exists!!', query);

        const dateTest = new Date(Date.now())
        const timeZone = 'Europe/Berlin'
        const zonedDate = utcToZonedTime(dateTest, timeZone)
        // zonedDate could be used to initialize a date picker or display the formatted local date/time

        // Set the output to "1.9.2018 18:01:36.386 GMT+02:00 (CEST)"
        const pattern = 'yyyy.MM.dd HH:mm:ss'
        const output = format(zonedDate, pattern, { timeZone: 'Europe/Berlin' })

        console.log("TEST DATE", output);

        let newDate = new Date(Date.now())
        // new Date(Date.now());
        let today = newDate.getDate();
        setDate(output)



        console.log("date", newDate);


        async function fetchData() {
            try {
                console.log("user ids", id);
                console.log("status", status);


                const result = await api.get(`${process.env.NEXT_PUBLIC_API_URL}/jobOffers/getOnejob/${pid}`);





                setEntries(result.data.data);



                /* setPosts(json.data.children.map(it => it.data)); */
            } catch (e) {
                console.error(e);
            }
        };
        fetchData();
    }, [loadDataOnlyOnce2, query]);

    useEffect(() => {
        entries.map((entry, i) => {
            let TagTable = [];
            for (const OneTag of entry.tagDescription) {
                console.log("ONETAG", OneTag.description);


                TagTable.push(OneTag.description);
                console.log("TAGTABLE", TagTable);

                setTags(TagTable)
            }

        }
        )
    }, [entries]);

    console.log("TAGS", tags)

    const updateJob = (values: any) => {
        try {

            api.patch(`${process.env.NEXT_PUBLIC_API_URL}/jobOffers/updatedJob/${pid}`, { values, tags, date });

        }
        catch (err) {
            console.log(
                "addJob", err
            );

        }

    }


    const onFinish = async (values: any) => {

 
        const newJob = new FormData();
        newJob.append("recruiter_id", user.user_id)
        newJob.append("available", values.available)
        newJob.append("remote", values.remote)
        newJob.append('organizationName', values.orgName)
        newJob.append('jobOffer_role', values.jobRole)
        newJob.append('jobOffer_description', values.jobDescription)
        newJob.append('country', values.jobCountry)
        newJob.append('city', values.jobCity)

        /*         newJob.append('updatedAt', date?.toDateString() || '{}') */
        /*         || '{}' */
        console.log("values", values);

        await updateJob(values)


    };



    const countries: any = []

    for (let value in countriesListData) {
        countries.push(
            <Option key={value}>{value}</Option>
        )

    }

    let valueCities: any = []

    const [selectedCity, selectedCityId] = useState(valueCities[0]);
    const [selectedCountry, selectedCountryId] = useState(countries[0]);
    const [chooseCity, setCityList] = useState<string[]>([])




    const changeCountry = (e: any) => {
        selectedCountryId({
            valueCities: [e],
            countries: [e][0]
        });
        setCityList(countriesListData[[e][0]])

    }




    const changeCity = (e: any) => {
        selectedCityId({
            valueCities: e,
        });
        console.log("changed city", e);

    }

    const job = jobList.jobsList.map((item, i) => (
        item

    ));

    const jobsCapitalized = job.map((eachJob: string, i) => (
        eachJob.charAt(0).toUpperCase() + eachJob.slice(1)
    ));



    const [selectedJobId, setSelectedJobId] = useState(job[0]);

    /* TAG*/
    const handleClose = (removedTag: any) => {
        const newTags = tags.filter((tag) => tag !== removedTag);
        console.log("removedTag", removedTag);

        api.delete(`${process.env.NEXT_PUBLIC_API_URL}/jobOffers/deleteSpecificTag/${removedTag}`);

        console.log("tags are", newTags);
        setTags(newTags);
    };


    const showInput = () => {
        setinputVisible(true);
    };

    const handleInputChange = (e: any) => {
        setInputValue(e.target.value);
    };

    const handleInputConfirm = () => {

        if (inputValue && tags.indexOf(inputValue) === -1) {
            tags = [...tags, inputValue];
        }
        console.log("tags are", tags);

        setTags(tags);
        setinputVisible(false);
        setInputValue('');

    };
    const saveInputRef = (input: any) => {
        input = input;
    };

    function switchOnChange(checked: boolean) {
        console.log(`switch to ${checked}`);
    }


    if (typeof window !== 'undefined' && loading) return null


    if (!session) {
        return <div><h1>Access Denied</h1>
            <p>
                You must be signed in to view this page
            </p></div>
    }

    if (entries.length < 0) {
        return <div><h1>Access Denied</h1>
            <Spin indicator={antIcon} /></div>
    }



    return (

        <div className="editBox_container">
            <SideNavbar />

            <div className="big_update_container">
                {entries?.map((entry, recordIdx) =>
                    <Form action="/public" encType="multipart/form-data" method="post"
                        labelCol={{
                            span: 4,
                        }}
                        wrapperCol={{
                            span: 14,
                        }}
                        layout="horizontal"
                        onFinish={onFinish}


                    >

                        <Form.Item label="Available" name="Available">

                            <Switch defaultChecked={entry.available === 1 ? true : false} onChange={switchOnChange} />
                        </Form.Item>
                        <Form.Item label="Remote" name="Remote">
                            <Select defaultValue={entry.remote}
                                onChange={e => setSelectedId(e)} allowClear

                            >
                                {remoteOption.map((item, i) => (
                                    <Select.Option value={item} key={i}>
                                        {item}
                                    </Select.Option>
                                ))}


                            </Select>
                        </Form.Item>

                        <Form.Item
                            name="orgName"

                        >
                            <Input defaultValue={entry.organizationName} onChange={event => setOrganizationName(event.target.value)} />
                        </Form.Item>

                        <Form.Item  name="jobRole"  >
                            <Select showSearch placeholder="Select a job" defaultValue={entry.jobOffer_role}
                                onChange={e => setSelectedJobId(e)} allowClear filterOption={true} >
                                {jobsCapitalized.map((item, i) => (
                                    <Select.Option value={item} key={i}>
                                        {item}
                                    </Select.Option>
                                ))}
                            </Select>

                        </Form.Item>


                        <Form.Item
                            name="jobDescription"

                        >
                            <Input defaultValue={entry.jobOffer_description} onChange={event => setJobOffer_description(event.target.value)} />
                        </Form.Item>






                        <Form.Item  name="Country" >

                            <Select style={{ width: 90 }}
                                defaultValue={entry.country}
                                onChange={e => changeCountry(e)} allowClear filterOption={true} >
                                {countries}

                            </Select>

                        </Form.Item>

                        <Form.Item name="City" >

                            <Select defaultValue={entry.city} allowClear onChange={e => changeCity(e)} filterOption={true}>
                                {chooseCity.map((item, i) => (
                                    <Select.Option value={item} key={i}>
                                        {item}
                                    </Select.Option>
                                ))}

                            </Select>

                        </Form.Item>

                        <div className='tags_list_container'>
                            {tags.map((item, i) =>

                                <div style={{ marginBottom: 16 }}>


                                    <Tag
                                        color="success"
                                        className="edit-tag"
                                        key={i}
                                        closable={i !== -1}
                                        onClose={() => handleClose(item)}


                                    >


                                        <Tooltip title={item} key={i}>
                                            {item.slice(0, 20)}
                                        </Tooltip>
                                    </Tag>
                                </div>)}
                        </div>



                        <Form.Item>
                            {tags.length <= 5 ? (
                                <div>


                                    {inputVisible && (
                                        <Input
                                            showCount maxLength={30}
                                            ref={saveInputRef}
                                            type="text"
                                            size="small"
                                            style={{ width: 278 }}
                                            value={inputValue}
                                            onChange={handleInputChange}
                                            onBlur={handleInputConfirm}
                                            onPressEnter={handleInputConfirm}
                                        />
                                    )}
                                    {!inputVisible && (
                                        <Tag color="success" onClick={showInput} className="site-tag-plus">
                                            <PlusOutlined /> Insert skills
                                        </Tag>
                                    )}
                                </div>
                            ) : (
                                <div> You have reached the amount of tags allowed.</div>
                            )} </Form.Item>



                        <Form.Item >

                            <Button htmlType="submit" >Update Offer</Button>

                        </Form.Item>
                    </Form>
                )}
            </div>
        </div>

    );
}
