import { GetServerSideProps, GetStaticProps, NextPage } from "next";
import Link from 'next/link';
import axios from 'axios'
import jwt_decode from 'jwt-decode'
import cookie from 'cookie'
import { jobOffer } from "../../../../entities";
import api from "../../../../services/interceptor";
import { getSession, useSession } from "next-auth/react";
import { SideNavbar } from "../../../../components/SideNavbar";
import { useState } from "react";
import { Menu, Switch } from 'antd';
import { MailOutlined, AppstoreOutlined, SettingOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router'
import { List, Typography, Divider } from 'antd';
const { SubMenu } = Menu;

const JobList: NextPage<{ jobs: jobOffer[] }> = ({ jobs }) => {
    const [jobMenu, setJobmenu] = useState<string>('');

    const ROUTE_POST_ID = "JobList/[id]";



    console.log("JBMENY", jobMenu);


    return (
        <div className="indexJob_container">
            <SideNavbar />
            <div className="jobList_container">
                <h1>Job List</h1>
                <Divider orientation="left">Your list of jobs</Divider>
                <List
                    
                    bordered
                    dataSource={jobs}
                    renderItem={job => (
                        <Link
                            href={{
                                pathname: ROUTE_POST_ID,
                                query: { id: job.jobOffer_id }
                            }}
                        >
                            <List.Item>
                                <Typography.Text mark>[Job : ]</Typography.Text> {job.jobOffer_role}
                                <img src="https://i.ibb.co/GRBw4sn/add.png" />
                            </List.Item>
                        </Link>
                    )}
                />


            </div>
        </div>
    )
}

export default JobList;

export const getServerSideProps: GetServerSideProps = async ({ query, req }) => {
    const userSession = await getSession({ req });
    console.log("userssion", userSession);


    const response = await axios.get(process.env.NEXT_PUBLIC_API_URL + '/jobOffers/getJobByRecruiter/' +
        query.by, {
        headers: {
            'Authorization': 'Bearer ' + userSession?.accessToken
        }
    }
    )



    console.log("RESPONSE", response.data.data);

    return {
        props: {
            jobs: response.data.data || null,

        }
    }
}

