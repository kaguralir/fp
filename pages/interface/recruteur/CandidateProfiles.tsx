
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { getSession, signIn, useSession } from 'next-auth/react';
import { GetStaticPaths, GetStaticProps, NextApiRequest, NextApiResponse, NextPage } from 'next';
import api from '../../../services/interceptor';
import { Menu, Dropdown, Button, message, Space, Tooltip, Divider, Tag } from 'antd';
import { DownOutlined, UserOutlined } from '@ant-design/icons';
import { jobOffer, ProfileCandidate, User } from '../../../entities';
import { Carousel, Radio } from "antd";
import { Card } from "antd";
import { DownloadOutlined } from "@ant-design/icons";
import { CheckCircleFilled, CloseCircleFilled, CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { Select } from 'antd';
import { SideNavbar } from '../../../components/SideNavbar';
import { compareAsc, parseISO } from 'date-fns'
import { zonedTimeToUtc, utcToZonedTime, format } from 'date-fns-tz'
import OneCandidate from '../../../components/jobs/OneCandidate';
import OneJobOffer from '../../../components/jobs/OneJobOffer';

const { Option } = Select;

interface Props {

    candidates: User;
}


export default function CandidatesProfiles() {

    const { data: session, status } = useSession()
    const loading = status === 'loading'
    const user = session?.user as any;

    const [jobs, getJobs] = useState<jobOffer[]>([]);
    const [job, setSelectedJob] = useState<jobOffer[]>([])

    const [candidatesForJob, setCandidatesForJob] = useState<ProfileCandidate[]>([])
    const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
    const [screen, setScreen] = useState<number>(920);




    const [width, setWidth] = useState<number>(window.innerWidth);

    function handleWindowSizeChange() {
        setWidth(window.innerWidth);
    }
    useEffect(() => {
        console.log("window.innerWidth", window.innerWidth);

        window.addEventListener('resize', handleWindowSizeChange);
        return () => {
            window.removeEventListener('resize', handleWindowSizeChange);
        }
    }, []);

    useEffect(() => {

        try {
            api.get(`${process.env.NEXT_PUBLIC_API_URL}/jobOffers/getJobByRecruiter/${user.user_id}`).then((response) => {
                if (response) {
                    console.log("session is", response.data.data);

                    getJobs(response.data.data)


                    return;
                };
                if (!response) {
                    console.log("no joboffers data");

                }
            });
        }
        catch (err) {
            console.log("err", err);

        }


    }, []);


    const getCandidates = (job: any) => {

        try {
            api.get(`${process.env.NEXT_PUBLIC_API_URL}/interest/jobCandidatesWithoutInterest/${job}`).then((response) => {
                if (response) {
                    console.log("candidates", response.data.data);

                    setCandidatesForJob(response.data.data)


                    return;
                };
                if (!response) {
                    console.log("no joboffers data");

                }
            });
        }
        catch (err) {
            console.log("err", err);

        }

    }

    useEffect(() => {
        console.log("job,", job);

        getCandidates(job)

    }, [job]);

    console.log("jobs", jobs);


    function onSearch(val: string) {
        console.log('search:', val);
    }



    if (typeof window !== 'undefined' && loading) return null


    if (!session) {
        return <div><h1>Access Denied</h1>
            <p>
                You must be signed in to view this page
            </p></div>
    }

    if (loading) {
        return <div><h1>Access Denied</h1>
            <Spin indicator={antIcon} /></div>
    }


    const interestPositive = (candidate_id: any, job: any, interest: any) => {
        try {

            api.post(`${process.env.NEXT_PUBLIC_API_URL}/interest/interestActivity`, { job, candidate_id, interest });
            console.log("values", { job, candidate_id });


        }
        catch (err) {
            console.log(
                "interestValue", err
            );

        }

    }


    const onFinish = async (candidate_id: any, job_id: any) => {
        const newInterest = new FormData();
        const interest = 1;
        newInterest.append("jobApplied_id", job_id);
        newInterest.append("candidateWhoApplied_id", candidate_id);
        newInterest.append("interest", interest.toString());
        await interestPositive(candidate_id, job_id, interest)
        getCandidates(job_id)



    };
    const onReject = async (candidate_id: any, job_id: any) => {
        const newInterest = new FormData();
        const interest = 0;
        newInterest.append("jobApplied_id", job_id);
        newInterest.append("candidateWhoApplied_id", candidate_id);
        newInterest.append("interest", interest.toString());

        await interestPositive(candidate_id, job_id, interest)
        getCandidates(job_id)



    };




    candidatesForJob?.map((oneCandidate) =>

        console.log("one", oneCandidate.fileName)

    );

    if (width >= 920) {
        return (
            <div className="container_candidateProfiles">
                <SideNavbar />
                <div className="header_recruiterHomePage">
                    <br />
                    <br />

                    <br />
                    <Select
                        showSearch
                        placeholder="Select a job search"
                        optionFilterProp="children"
                        onSearch={onSearch}
                        onChange={e => setSelectedJob(e)}                >
                        {jobs?.map((OneJob, i) =>
                            <Option name="jobOffer_id" key={OneJob.jobOffer_id} value={OneJob.jobOffer_id} allowClear filterOption={true}>{OneJob.jobOffer_role}</Option>
                        )}
                    </Select>
                    <div className="secondContainerRecruiterHome">

                        {candidatesForJob?.map((oneCandidate, i) =>
                            <div key={i} className="card-width">


                                <div className="card-container">





                                    <Carousel dotPosition={"top"}>
                                        {/*                                 localhost:8000/uploads/thumbnails/bfcc54e3-ca3e-489b-8454-18295d875ca0.png
                                         */}

                                        {oneCandidate.images ? oneCandidate.images?.map((oneImage, i) =>
                                            <img

                                                className="round"
                                                src={`${process.env.NEXT_PUBLIC_URL}/uploads/thumbnails/${oneImage}`} />
                                        )



                                            :

                                            <img
                                                className="round"
                                                src="https://cdn-icons.flaticon.com/png/512/2179/premium/2179272.png?token=exp=1643296938~hmac=55be32eea58bbc3997171fe32aa3857c"
                                            />

                                        }
                                    </Carousel>


                                    <h3>{oneCandidate.name}</h3>
                                    <p>Remote: {oneCandidate.remote} <br /> </p>

                                    <h3>
                                        {oneCandidate.job_title}<br /> {oneCandidate.description}
                                    </h3>

                                    <div className="skills">
                                        <Divider orientation="left">Skills : </Divider>
                                        <ul>
                                            <li>{oneCandidate.skill1}</li>
                                            <li>{oneCandidate.skill2}</li>
                                            <li>{oneCandidate.skill3}</li>

                                        </ul>
                                        <div>
                                            {" "}
                                            <Divider orientation="left">Soft Skills</Divider>
                                            <Tag color="lime">{oneCandidate.softSkill1}</Tag>
                                            <Tag color="green">{oneCandidate.softSkill2}</Tag>
                                            <Tag color="cyan">{oneCandidate.softSkill3}</Tag>


                                            <Divider orientation="left">Hobbies</Divider>
                                            <Tag color="magenta">{oneCandidate.hobby1}</Tag>
                                            <Tag color="red">{oneCandidate.hobby2}</Tag>
                                            <Tag color="volcano">{oneCandidate.hobby3}</Tag>
                                        </div>

                                    </div>
                                    <div className="buttons">
                                        <Button
                                            onClick={() => onFinish(oneCandidate.user_id, job)}

                                            type="primary"
                                            icon={<CheckOutlined />}
                                            size={"large"}
                                        />
                                        <Button

                                            onClick={() => onReject(oneCandidate.user_id, job)}

                                            value={oneCandidate.user_id}
                                            type="primary"
                                            shape="circle"
                                            icon={<CloseOutlined />}
                                            size={"large"}
                                        />

                                    </div>
                                </div>
                            </div>
                        )}
                    </div>

                </div>

            </div>
        )
    }
    else {
        return (
            <OneJobOffer />

        )
    }


}


