import { Button, Space, Spin, Input, Select, Cascader, DatePicker } from "antd";
import Form from "antd/lib/form/Form";
import { useSession } from "next-auth/react";
import React, { useCallback, useEffect, useState } from "react";
import { SideBarCandidat } from "../../../components/SideBarCandidat";
import { SearchedJob } from "../../../entities";
import api from "../../../services/interceptor";
import { EditOutlined } from "@ant-design/icons";
import { userInfo } from "os";
import moment, { Moment } from "moment";
import CompleteProfiles from "../../../components/profiles/CompleteProfiles";

const { Option } = Select;
interface Props {
    values: SearchedJob;
}

export default function EditBox() {
    const [entries, setEntries] = useState<SearchedJob[]>([])
    const [description, setDesc] = useState<string>('')
    const [job_title, setJob_title] = useState<string>('')

    const [skill1, setSkill1] = useState<string>('')
    const [city, setCity] = useState<string>('')
    const [beginDate, setDate] = useState<Moment>()
    const dateFormat = "YYYY-MM-DD";

    const [indexToEdit, setIndexToEdit] = useState(-1);
    const { data: session, status } = useSession()
    const loading = status === 'loading'
    const user = session?.user as any;
    const id = user?.user_id;

    const loadDataOnlyOnce2 = useCallback(() => {
        console.log(`I need ${status}!!`);
        if (status === 'authenticated') {
            console.log("status is authenticated");
            console.log("user is", user);


            return user;
        }
    }, [status]);
    const noUser = () => {
        console.log("noUser");
        if (!id || !user || !session) {
            return <div><Space>
                <Spin size="large" />
            </Space> </div>
        }
        if (!user || user.isLoggedIn === false) {
            return <div>Loading...</div>
        }
    };
    useEffect(() => {
        loadDataOnlyOnce2();
        noUser();
        async function fetchData() {
            try {
                console.log("user ids", id);
                console.log("status", status);



                const result = await api.get(`${process.env.NEXT_PUBLIC_API_URL}/searchedJob/getSearchJob/${user.user_id}`);
                /* const json = await result.data.json();
                const json2 = await result.json(); */
                console.log("RESULT", result);
                console.log("result", result);
                console.log(".data.data.description", result.data.data[0].skill1);

                setDesc(result.data.data[0].description)
                setEntries(result.data.data);

                /* setPosts(json.data.children.map(it => it.data)); */
            } catch (e) {
                console.error(e);
            }
        };
        fetchData();
    }, [loadDataOnlyOnce2]);


    // if (entries.length < 1) {
    //     return <div><h1>No data</h1>
    //         <Space>
    //             <Spin size="large" />
    //         </Space> </div>
    // }

    const addSearch = async (job_title: any, description: any, city: any, skill1: any, beginDate: any) => {
        try {

            console.log("VALUES==========>", job_title, description, city, skill1);

            await api.post('/searchedJob/addSearch', { job_title, description, city, skill1, beginDate });

        }
        catch (err) {
            console.log(
                "addSearch", err
            );

        }

    }

    const onFinish = async () => {
        console.log("entries send", entries);


        console.log("desc send", description);
        console.log("desc send", beginDate);


        await addSearch(job_title, description, city, skill1, beginDate)

    }

    console.log("entries", entries);
    console.log("entries lefnth", entries.length);






    return (
        <>
            {entries.length > 0 ?
                <div className="editBox_container">
                    <SideBarCandidat />
                    <div className="big_update_container" >

                        <h1> Your search informations</h1>
                        {entries?.map((entry, recordIdx) => (

                            <div className="update_container">
                                <div className="update_input_container">
                                    <Input
                                        name="job_title"
                                        type="text"
                                        disabled={recordIdx !== indexToEdit}


                                        onChange={(val: any) => {
                                            setJob_title(val.target.value);
                                        }}
                                        onBlur={() => {
                                            setIndexToEdit(-1);
                                        }}
                                        addonBefore="Job Title"
                                        addonAfter={<EditOutlined onClick={() => {
                                            setIndexToEdit(recordIdx);
                                        }} />}
                                        defaultValue={entry.job_title}
                                    />
                                </div>

                                <div className="update_input_container">
                                    <Input
                                        name="desc"
                                        type="text"
                                        placeholder={entry.description}
                                        disabled={recordIdx !== indexToEdit}


                                        onChange={(val: any) => {
                                            val = val.target.value;
                                            setDesc(val);
                                        }}
                                        onBlur={() => {
                                            setIndexToEdit(-1);
                                        }}
                                        addonBefore="Description"
                                        addonAfter={<EditOutlined onClick={() => {
                                            setIndexToEdit(recordIdx);
                                        }} />}

                                    />
                                </div>

                                <div className="update_input_container">
                                    <Input
                                        name="city"
                                        type="text"
                                        placeholder={entry.city}
                                        disabled={recordIdx !== indexToEdit}


                                        onChange={(val: any) => {

                                            setCity(val.target.value);
                                        }}
                                        onBlur={() => {
                                            setIndexToEdit(-1);
                                        }}
                                        addonBefore="City"
                                        addonAfter={<EditOutlined onClick={() => {
                                            setIndexToEdit(recordIdx);
                                        }} />}
                                    />
                                </div>

                                <div className="update_input_container">
                                    <Input
                                        name="job_title"
                                        type="text"
                                        placeholder={entry.skill1}
                                        disabled={recordIdx !== indexToEdit}


                                        onChange={(val: any,) => {

                                            setSkill1(val.target.value);
                                        }}
                                        onBlur={() => {
                                            setIndexToEdit(-1);
                                        }}
                                        addonBefore="Skill"
                                        addonAfter={<EditOutlined onClick={() => {
                                            setIndexToEdit(recordIdx);
                                        }} />}
                                    />
                                </div>

                                <div className="update_input_container">



                                    {/*   date.format('YYYY-MM-DD') defaultValue={moment(date).format("YYYY-MM-DD")} String(date) */}
                                    <DatePicker name="beginDate" defaultValue={moment(beginDate)} format={dateFormat}
                                        onChange={(date: any, dateString: any) => setDate(dateString)}
                                        onBlur={() => {
                                            setIndexToEdit(-1);
                                        }} />

                                </div>

                            </div>
                        ))}

                        <Button type="primary" htmlType="submit" onClick={onFinish}>Mettre à jour le profil</Button>
                    </div>

                </div>
                :
                <div><CompleteProfiles /></div>}
        </>
    );
}