import React, { useState, useEffect, useCallback } from 'react';
import { useSession } from 'next-auth/react';
import api from '../../../services/interceptor';
import {  Button, Divider, Tag } from 'antd';
import { jobOffer, User } from '../../../entities';
import { Carousel } from "antd";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";

import { Select } from 'antd';
import { SideBarCandidat } from '../../../components/SideBarCandidat';
import OneCandidate from '../../../components/jobs/OneCandidate';
import moment from 'moment';

const { Option } = Select;



export default function JobOffers() {

    const { data: session, status } = useSession()
    const loading = status === 'loading'
    const user = session?.user as any;

    const [jobs, getJobs] = useState<jobOffer[]>([]);
    const [job, setSelectedJob] = useState<jobOffer[]>([])
    const [pictures, setPictures] = useState<string>();


    const [screen, setScreen] = useState<number>(920);




    const [width, setWidth] = useState<number>(window.innerWidth);

    function handleWindowSizeChange() {
        setWidth(window.innerWidth);
    }
    useEffect(() => {
        console.log("window.innerWidth", window.innerWidth);

        window.addEventListener('resize', handleWindowSizeChange);
        return () => {
            window.removeEventListener('resize', handleWindowSizeChange);
        }
    }, []);
    const handleWindowSizeChange2 = () => {
        console.log("window.innerWidth", window.innerWidth);

        setScreen(window.innerWidth);
    };


    const [candidatesForJob, setCandidatesForJob] = useState<User[]>([])

    /* 
        if (typeof window !== 'undefined' && loading) return null
    
     */
    const loadDataOnlyOnce2 = useCallback(() => {
        console.log(`I need ${status}!!`);
        if (status === 'authenticated') {
            console.log("status is authenticated");
            console.log("user is", user);


            return user;
        }
    }, [status]);
    async function fetchjobsforCandidates() {
        try {
            api.get(`${process.env.NEXT_PUBLIC_API_URL}/jobOffers/getJobTestPerJob`, user.user_id).then((response) => {
                if (response) {
                    console.log("get job is", response.data.data);

                    getJobs(response.data.data)


                    return;
                };
                if (!response) {
                    console.log("no joboffers data");
                    return

                }
            });
        }
        catch (err) {
            console.log("err", err);

        }
    }

    useEffect(() => {
        loadDataOnlyOnce2();
        fetchjobsforCandidates();
    }, []);



    const interestPositive = (candidate_id: any, job: any, interest: any) => {
        try {

            api.post(`${process.env.NEXT_PUBLIC_API_URL}/interest/interestActivity`, { job, candidate_id, interest });
            console.log("values", job, candidate_id, interest);



        }
        catch (err) {
            console.log(
                "interestValue", err
            );

        }

    }


    const onFinish = async (candidate_id: any, job_id: any) => {
        const newInterest = new FormData();
        const interest = 1;
        newInterest.append("jobApplied_id", job_id);
        newInterest.append("candidateWhoApplied_id", candidate_id);
        newInterest.append("interest", interest.toString());

        await interestPositive(candidate_id, job_id, interest)
        await fetchjobsforCandidates()

    };
    const onReject = async (candidate_id: any, job_id: any) => {
        const newInterest = new FormData();
        const interest = 0;
        newInterest.append("jobApplied_id", job_id);
        newInterest.append("candidateWhoApplied_id", candidate_id);
        newInterest.append("interest", interest.toString());

        await interestPositive(candidate_id, job_id, interest)
        await fetchjobsforCandidates()


    };

    {
        jobs?.map((job, i) => {
            job.tagDescription?.map((oneTag, i) => {
                oneTag.map((oneTag, i) =>

                    console.log("one", oneTag.description)


                )
            }

            )
        }
        )
    }
    if (width >= 920) {
        return (


            <div className="container_candidateProfiles">
                <SideBarCandidat />
                <div className="header_recruiterHomePage">
                    <br />
                    <br />

                    <br />
{jobs.length > 0 ?
                    <div className="secondContainerRecruiterHome">

                        {jobs?.map((job, i) =>
                            <div key={i} className="card-width">


                                <div className="card-container">

                                    {job.images?.map((oneImage, i) =>
                                        <Carousel dotPosition={"top"}>
                                            

                                               

                                                    <img className="round" src={oneImage ? `${process.env.NEXT_PUBLIC_URL}uploads/thumbnails/${oneImage}` : 'https://images.unsplash.com/photo-1599508704512-2f19efd1e35f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80'} />


                                        </Carousel>
                                    )}
                                    <h2> {job.jobOffer_role}</h2>
                                    <h6>{job.organizationName}</h6>
                                    <p>
                                        {job.city} <br />{job.country}
                                    </p>
                                    <p>
                                        Remote: <br />{job.remote}
                                    </p>
                                    <p>
                                       Last updated: <br /> {moment(job.updatedAt).fromNow()}
                                    </p>

                                    <div className="skills">
                                        <Divider orientation="left">Informations</Divider>
                                        <ul>
                                            <li>{job.jobOffer_description}</li>
                                        </ul>
                                        <h6>Tags</h6>
                                        {job.tagDescription ?
                                            <div>
                                                {" "}

                                                <Divider orientation="left"></Divider>
                                                {
                                                    job.tagDescription?.map((oneTag, i) =>
                                                        <div>{
                                                            oneTag.map((oneTag, i) =>
                                                                <Tag color="magenta">{oneTag.description}</Tag>



                                                            )
                                                        }
                                                        </div>

                                                    )
                                                }

                                            </div>

                                            :

                                            <p>no tag</p>
                                        }
                                    </div>
                                    <div className="buttons">
                                        <Button
                                            onClick={() => onFinish(user.user_id, job.jobOffer_id)}

                                            type="primary"
                                            icon={<CheckOutlined />}
                                            size={"large"}
                                        />
                                        <Button


                                            onClick={() => onReject(user.user_id, job.jobOffer_id)}
                                            type="primary"
                                            shape="circle"
                                            icon={<CloseOutlined />}
                                            size={"large"}
                                        />

                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
:
                                    <div> Please complete your job search profile before accessing data. </div>}
                </div>

            </div>
        );
    }
    else {
        return (
            <OneCandidate />

        )
    }
}




