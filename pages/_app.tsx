import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { getSession, SessionProvider } from 'next-auth/react'
import '../styles/Navbar.css'
import '../styles/styles.css'
import '../styles/card.css'
import '../styles/mobileView.css'
import '../styles/form.css'
import App from 'next/app'
import "../services/interceptor"

import '../styles/antd.css'
import '../styles/messages.css'
import '../styles/profile.css'
import '../styles/editForm.css'
import '../styles/editProfile.css'
import '../styles/completeProfiles.css'









function MyApp({ Component, pageProps: { session, ...pageProps } }: AppProps) {

  return <SessionProvider session={session}  >

    <Component {...pageProps} />
  </SessionProvider>
}


/* MyApp.getInitialProps = async (context: any) => {
  const appProps = await App.getInitialProps(context)
  const session = await getSession(context)

  return {
    ...appProps,
    session
  }
}*/

export default MyApp 