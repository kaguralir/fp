import axios from "axios";
import NextAuth, { User } from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials";
import api from "../../../services/interceptor";
import { signOut } from "next-auth/react";
import { JWT } from "next-auth/jwt";
import jwtDecode from "jwt-decode";



async function refreshAccessToken(token: JWT) {

    try {
        const response = await axios.post(process.env.NEXT_PUBLIC_API_URL + '/user/refreshToken', {}, {
            headers: {
                "Authorization": `bearer ${token.refresh_token}`
            },
        })


        return {
            ...token,
            accessToken: response.data.token,
            refreshToken: response.data.refresh_token ?? token.refresh_token, // Fall back to old refresh token
        }
    } catch (error) {
        console.log(error)
        signOut()
        return {
            ...token,
            error: "RefreshAccessTokenError",
        }
    }
}

export default NextAuth({
    callbacks: {
        async jwt({ token, user }) {
            if (user) {
                return {

                    accessToken: user.token,
                    user: user.user
                }

            }

            return token;
        },
        async session({ session, token }) {
            session.accessToken = token.accessToken;
            session.user = token.user as any;

            return session;

        }

    },
    providers: [

        CredentialsProvider({

            name: "Credentials",
            credentials: {
                email: { label: "Email", type: "text", placeholder: "Your email..." },
                password: { label: "Password", type: "password", placeholder: "Your Password..." }
            },
            async authorize(credentials, req) {
                try {
                    const response = await api.post("/user/login", credentials);
                    const data = response.data;
                    if (data.user && data.token) {
                        return data;
                    }
                    return null;
                } catch (error) {
                    console.log("error", error);
                    return null;
                }
            }
        })

    ],
    secret: process.env.SECRET,
    /*     session: {
    
            strategy: 'jwt',
            maxAge: 30 * 24 * 60 * 60,
        } */
})
