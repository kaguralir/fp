import axios from "axios";
import { getSession } from "next-auth/react";
import Router from 'next/router'

const api = axios.create({
	baseURL: process.env.NEXT_PUBLIC_API_URL
})//créer une instance
api.interceptors.response.use((response) => {

	return response
}, (error) => {
	if (error.response) {

		console.log("error response", error.response);


	} else if (error.request) {

		console.log("error request", error.request);

	} else if (error.message) {

		console.log("error message", error.message);

	}

})
api.interceptors.request.use(async (config: any) => {

	const session = await getSession();
	console.log("session is", session);

	if (session) {

		config.headers.authorization = 'bearer ' + session.accessToken;
	}
	return config;
});



export default api
